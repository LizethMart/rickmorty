import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-componente-uno',
  templateUrl: './componente-uno.component.html',
  styleUrls: ['./componente-uno.component.css']
})
export class ComponenteUnoComponent implements OnInit {

  constructor() { }
  //Areglo
  amigos=['Day','Liz','emi'];
  arr = ['1','2','3','4','5','6','7','8','9'];

  //IF

  nota=75;
  edad=19;


  //objeto
  alumnos={
    nombre:'lizeth',
    edad:18,
  };

  //objeto anidado

  public alumnoss={
    a1:{
      nombre:'lizeth',
      edad:18,
    },
    a2:{
      nombre:'Areli',
      edad:18,
    },
  };
  estudiantes=Object.values(this.alumnoss);

  //aray de objetos

  carros = [
    {
      marcavehiculo:'Nissan',
      color:'rojo',
      numPuertas:5,
    },
    {
      cantante:'BadBunny',
      edad:28,
      sexo:'m'
    },
  ];

  ngOnInit(): void {
    for (let amigos = 0; amigos <=10; amigos++) {
  }
  }

}
